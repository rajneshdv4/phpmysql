CREATE DATABASE IF NOT EXISTS `devopstest` ;
CREATE USER 'root'@'127.0.0.1' IDENTIFIED WITH mysql_native_password BY 'rajdevops';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'127.0.0.1';
CREATE TABLE IF NOT EXISTS `dockerSample` (name varchar(20) DEFAULT NULL, age varchar(3) NOT NULL,addr varchar(40) NOT NULL,pin int(3) NOT NULL);
INSERT INTO `dockerSample` (name,age,addr,pin) VALUES ('Rajnesh','30','bangalore','201516');
